import Phaser from "phaser";
import config from "visual-config-exposer";

let box, music, background;
let counter, time, timeText, timeDisplayText;
let scoreText,
  generalText,
  generalText2,
  scoreDisplayText,
  scoreArray = config.game.scoreText,
  score = 0;
let emitter1, emitter2, emitter3, emitter4;
let particles1, particles2, particles3, particles4;

class Game extends Phaser.Scene {
  constructor() {
    super("playGame");
    this.GAME_WIDTH = 740;
    this.GAME_HEIGHT = 900;
  }

  create() {
<<<<<<< src/js/Game.js
    background = this.add.image(0, 0, 'bg');
=======
    background = this.add.image(0, 0, "bg");
>>>>>>> src/js/Game.js
    background.setOrigin(0, 0);
    background.setScale(config.preGame.bgImgScaleX, config.preGame.bgImgScaleY);
    this.matter.world.setBounds();
    this.createBox();

    music = this.sound.add("flip");

    scoreText = this.add.text(80, 90, `${(score = 0)}`, {
      fontSize: "84px",
      fontFamily: "Helvetica",
      fontWeight: "bold",
      fill: config.preGame.textColor,
      boundsAlignH: "center",
      boundsAlignV: "top",
      align: "center",
    });

    scoreDisplayText = this.add.text(85, 170, "SCORE", {
      fontSize: "22px",
      fontFamily: "Helvetica",
      fontWeight: "bold",
      fill: config.preGame.textColor,
      boundsAlignH: "center",
      boundsAlignV: "top",
      align: "center",
    });

    particles1 = this.add.particles("red");
    particles2 = this.add.particles("blue");
    particles3 = this.add.particles("green");
    particles4 = this.add.particles("yellow");

    // emitter.emitParticle(100, 200, 200);

    time = config.game.gameTime;

    generalText = this.add.text(230, 30, `${config.game.instruction1}`, {
      fontSize: "24px",
      fontFamily: "Helvetica",
      fontWeight: "bold",
      fill: config.preGame.textColor,
      boundsAlignH: "center",
      boundsAlignV: "top",
      align: "center",
    });

    generalText2 = this.add.text(160, 60, `${config.game.instruction2}`, {
      fontSize: "24px",
      fontFamily: "Helvetica",
      fontWeight: "bold",
      fill: config.preGame.textColor,
      boundsAlignH: "center",
      boundsAlignV: "top",
      align: "center",
    });

    timeText = this.add.text(500, 90, `${time}`, {
      fontSize: "84px",
      fontFamily: "Helvetica",
      fontWeight: "bold",
      fill: config.preGame.textColor,
      boundsAlignH: "center",
      boundsAlignV: "top",
      align: "center",
    });

    timeDisplayText = this.add.text(495, 170, "TIME LEFT", {
      fontSize: "22px",
      fontFamily: "Helvetica",
      fontWeight: "bold",
      fill: config.preGame.textColor,
      boundsAlignH: "center",
      boundsAlignV: "top",
      align: "center",
    });

    this.gameTimer = this.time.addEvent({
      delay: 1000,
      callback: function () {
        time--;
        timeText.setText(`${time}`);
        if (time <= 0) {
          this.scene.start("endGame", { score, message: "You Win" });
        }
      },
      callbackScope: this,
      loop: true,
    });

    background.setInteractive();
<<<<<<< src/js/Game.js
    background.on('pointerdown', this.bounce);
=======
    background.on("pointerdown", this.bounce);
>>>>>>> src/js/Game.js
  }

  update() {
    if (box && !counter) {
      if (
        Math.abs(Math.floor(box.angle)) <= 1 &&
        Math.abs(Math.floor(box.body.angularVelocity)) <= 0.6 &&
        Math.abs(Math.floor(box.body.velocity.x)) <= 0.6 &&
        Math.abs(Math.floor(box.body.velocity.y)) <= 0.6
      ) {
        counter = 1;
        // this.tweens.add({
        //   targets: box,
        //   scaleX: 0,
        //   scaleY: 0,
        //   duration: 50,
        // });
        emitter1 = particles1.createEmitter({
          x: 100,
          y: 200,
          speed: 500,
          gravityY: 500,
          scale: { start: 0.1, end: 0 },
          lifespan: 1000,
          // delay: 100,
        });

        emitter2 = particles2.createEmitter({
          x: 300,
          y: 200,
          speed: 500,
          gravityY: 500,
          scale: { start: 0.1, end: 0 },
          lifespan: 1000,
          // delay: 300,
        });

        emitter3 = particles3.createEmitter({
          x: 450,
          y: 200,
          speed: 500,
          gravityY: 500,
          scale: { start: 0.1, end: 0 },
          lifespan: 1000,
          // delay: 200,
        });

        emitter4 = particles4.createEmitter({
          x: 650,
          y: 200,
          speed: 500,
          gravityY: 500,
          scale: { start: 0.1, end: 0 },
          lifespan: 1000,
          // delay: 400,
        });
        emitter1.explode(100, 200, 200);
        emitter2.explode(100, 300, 200);
        emitter3.explode(100, 400, 200);
        emitter4.explode(100, 500, 200);
        setTimeout(() => {
          box.destroy();
          scoreText.setText(`${++score}`);
          music.play();
          generalText.x = 250;
          generalText.scale = 2;
          generalText.setText(
            `${scoreArray[`text${Math.floor(Math.random() * 6 + 1)}`]}`
          );
          generalText2.setText("");
        }, 100);
        setTimeout(() => {
          this.createBox();
        }, 200);
      }
    }
  }

  bounce() {
    if (box) {
      box.setVelocityY(-7);
      box.setAngularVelocity(box.body.angularVelocity + 0.04);
    }
  }

  createBox() {
    const angle = [90, 180, 270];
    counter = 0;
    box = this.matter.add.image(
      this.GAME_WIDTH / 2,
      this.GAME_HEIGHT / 2,
      "box",
      null
    );
    box.setDisplaySize(225, 225);
    box.setFrictionAir(0.01);
    box.setBounce(0.6);
    box.body.collideWorldBounds = true;
    box.angle = angle[Math.floor(Math.random() * 3)];
    // box.setInteractive();
    // box.on('pointerdown', this.bounce);
  }
}

export default Game;
